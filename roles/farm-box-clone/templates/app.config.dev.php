<?php
return [
    'components' => [
        'mongodb' => [
            'class' => '\yii\mongodb\Connection',
            'dsn' => '{{ app_mongodb_dsn }}',
        ],
        'db' => [
            'class'     => 'yii\db\Connection',
            'dsn'       => '{{ app_pgsql_dsn }}',
            'username'  => '{{ app_pgsql_user }}',
            'password'  => '{{ app_pgsql_pass }}',
            'charset'   => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => true,
        ],
    ],
];

